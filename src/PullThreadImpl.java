import java.util.Random;

public class PullThreadImpl implements Runnable {
    private final String name;
    public static int t1 = 1000, t2 = 2000;

    PullThreadImpl(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        Utilities.printCyan(this.name + " has acquired the job.");
        int millis = new Random().nextInt((t2 - t1)) + t1;

        try {
            Thread.sleep(millis);
            Utilities.printYellow(this.name + " has been working for " + millis + " milliseconds.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Utilities.printRed(this.name + " has finished the job.");
    }
}