import java.util.Random;

public class MutexImpl implements Runnable {
    private static final Object mutex = new Object();
    public static int t1 = 1000, t2 = 2000;

    @Override
    public void run() {
        Utilities.printCyan(Thread.currentThread().getName() + " is waiting to acquire a mutex.");

        synchronized (MutexImpl.mutex) {
            Utilities.printCyan(Thread.currentThread().getName() + " has acquired mutex.");
            int millis = new Random().nextInt((t2 - t1)) + t1;

            try {
                Thread.sleep(millis);
                Utilities.printYellow(Thread.currentThread().getName() + " has been working for " + millis + " milliseconds.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Utilities.printRed(Thread.currentThread().getName() + " has released the mutex.");
        }
    }
}