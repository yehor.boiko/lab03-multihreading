import java.util.Random;
import java.util.concurrent.Semaphore;

public class SemaphoreImpl implements Runnable {
    public static int t1 = 1000, t2 = 2000;
    Semaphore semaphore;

    SemaphoreImpl(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        Utilities.printCyan(Thread.currentThread().getName() + " is waiting to acquire semaphore.");

        // Try to acquire a semaphore.
        try {
            semaphore.acquire();
            Utilities.printCyan(Thread.currentThread().getName() + " has acquired semaphore.");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        try {
            int millis = new Random().nextInt((t2 - t1)) + t1;
            Thread.sleep(millis);
            Utilities.printYellow(Thread.currentThread().getName() + " has been working for " + millis + " milliseconds.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Release the semaphore.
        Utilities.printRed(Thread.currentThread().getName() + " has released the semaphore.");
        semaphore.release();
    }
}