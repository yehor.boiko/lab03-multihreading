import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    protected static Scanner in;

    public static void main(String[] args) {
        int selectedOption;
        Main.in = new Scanner(System.in);

        // Run kernel. 
        while(true) {
            Main.displayMainContextMenu();
            selectedOption = Main.in.nextInt();
            // Exit the calculator.
            if(selectedOption == 5) break;
            // Check if the operation exists.
            else if(selectedOption < 1 || selectedOption > 5)
                Utilities.printRed("Такого пункту не існує!");

            // Choose the algorithm.
            switch(selectedOption) {
                case 1:
                    // Run the mutex synchronization.
                    Main.runMutex();
                    break;
                case 2:
                    // Run the semaphore synchronization.
                    Main.runSemaphore();
                    break;
                case 3:
                    // Run the atomic synchronization.
                    Main.runAtomic();
                    break;
                case 4:
                    // Run the pull thread synchronization.
                    Main.runPullThread();
                    break;
            }
        }

        Main.in.close();
    }

    public static void runMutex() {
        Utilities.printYellow("Enter amount of threads: ");
        int n = Main.in.nextInt();
        Utilities.printYellow("Enter interval t1: ");
        int t1 = Main.in.nextInt();
        Utilities.printYellow("Enter interval t2: ");
        int t2 = Main.in.nextInt();

        MutexImpl.t1 = t1;
        MutexImpl.t2 = t2;

        Thread[] threads = new Thread[n];
        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(new MutexImpl());
            thread.setName("IamThread-" + i);
            threads[i] = thread;
            thread.start();
        }

        // Wait until all threads are finished to continue and go back to "Main".
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void runSemaphore() {
        Utilities.printYellow("Enter amount of threads: ");
        int n = Main.in.nextInt();
        Utilities.printYellow("Enter interval t1: ");
        int t1 = Main.in.nextInt();
        Utilities.printYellow("Enter interval t2: ");
        int t2 = Main.in.nextInt();

        SemaphoreImpl.t1 = t1;
        SemaphoreImpl.t2 = t2;

        Thread[] threads = new Thread[n];
        Semaphore semaphore = new Semaphore(4);

        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(new SemaphoreImpl(semaphore));
            thread.setName("IamThread-" + i);
            threads[i] = thread;
            thread.start();
        }

        // Wait until all threads are finished to continue and go back to "Main".
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void runAtomic() {
        Utilities.printYellow("Enter amount of threads: ");
        int n = Main.in.nextInt();

        AtomicImpl AtomicObj = new AtomicImpl(new AtomicInteger(0));
        Thread[] threads = new Thread[n];

        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(AtomicObj);
            thread.setName("IamThread-" + i);
            threads[i] = thread;
            thread.start();
        }

        // Wait until all threads are finished to continue and go back to "Main".
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Utilities.printRed("Final atomic value: " + AtomicObj.getValue());
    }

    public static void runPullThread() {
        Utilities.printYellow("Enter the overall amount of threads in the pull: ");
        int k = Main.in.nextInt();
        Utilities.printYellow("Enter the number of threads to execute: ");
        int n = Main.in.nextInt();
        Utilities.printYellow("Enter interval t1: ");
        int t1 = Main.in.nextInt();
        Utilities.printYellow("Enter interval t2: ");
        int t2 = Main.in.nextInt();

        PullThreadImpl.t1 = t1;
        PullThreadImpl.t2 = t2;

        ExecutorService executorService = Executors.newFixedThreadPool(k);
        for (int i = 0; i < n; i++) {
            executorService.execute(new PullThreadImpl("IamThread-" + i));
        }
        executorService.shutdown();

        // Wait until all threads are finished to continue and go back to "Main".
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void displayMainContextMenu() {
        Utilities.printYellow("Оберiть пункт:");
        Utilities.printCyan("1. Синхорізація м'ютекс");
        Utilities.printCyan("2. Синхорізація семафори");
        Utilities.printCyan("3. Синхронізація атомарна");
        Utilities.printCyan("4. Синхронізація кеш потоків");
        Utilities.printCyan("5. Вийти з програми");
    }
}