public class Utilities {
    // ANSI escape codes.
    private static final String YELLOW = "\u001B[33m";
    private static final String RED = "\u001B[31m";
    private static final String CYAN = "\u001B[36m";
    private static final String RESET = "\u001B[0m";

    public static void printRed(String message) {
        System.out.println(Utilities.RED + message + Utilities.RESET);
    }
    public static void printYellow(String message) {
        System.out.println(Utilities.YELLOW + message + Utilities.RESET);
    }
    public static void printCyan(String message) {
        System.out.println(Utilities.CYAN + message + Utilities.RESET);
    }
}