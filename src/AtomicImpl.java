import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicImpl implements Runnable {
    private final AtomicInteger value;

    public AtomicInteger getValue() {
        return this.value;
    }

    AtomicImpl(AtomicInteger initialValue) { this.value = initialValue; }

    @Override
    public void run() {
        Utilities.printCyan("%s incremented the atomic value: %s".formatted(Thread.currentThread().getName(), this.value.incrementAndGet()));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}