# Дослідження методів багатопоточності та засобів паралельного API Java при розробки програмних компонентів розподіленної системі

# GitLab

1.  GitLab Commit Tree

    <p align="center">  
        ![Рис. 1. GitLab Commit Tree](images/gitlabTree.png)
    </p>

    <p align="center">
        Рис. 1. <b>GitLab Commit Tree</b>
    </p>

# Результати

- [x] Завдання 1 (обов'язкове)
- [x] Завдання 2 (командне 75-100%)

# Завдання 1 (обов'язкове)

1. `git clone https://gitlab.com/yehor.boiko/lab03-multihreading.git`
- Clone the remote repository

2. `git config --local user.name "Yehor Boiko"`
- Set the user name for the local repository

3. `git config --local user.email "yehor.boiko@hneu.net"`
- Set the user email for the local repository

4. `git add .`
- Add all changed files to the staging area

5. `git commit -m "Initialize project. Created context menu."`
- Commit the changes with a message

6. `git branch -v`
- List branches with last commit information

7. `git push origin main`
- Push the main branch to the remote repository

8. `git checkout -b Mutex`
- Create a new branch named 'Mutex' and switch to it

9. `git add .`
   `git commit -m "Added Mutex."`
- Add changes and commit with a message

10. `git push origin Mutex`
- Push the Mutex branch to the remote repository

11. `git switch main`
- Switch to the main branch

12. `git merge --no-ff Mutex`
- Merge the Mutex branch into main

13. `git push origin main`
- Push the merged changes to the remote main branch

14. `git checkout -b Semaphore`
- Create a new branch named 'Semaphore' and switch to it

15. `git add .`
    `git commit -m "Added Semaphore."`
- Add changes and commit with a message

16. `git switch Mutex`
- Switch to the Mutex branch

17. `git switch main`
- Switch to the main branch

18. `git switch Semaphore`
- Switch to the Semaphore branch

19. `git push origin Semaphore`
- Push the Semaphore branch to the remote repository

20. `git switch main`
    `git merge --no-ff Semaphore`
- Merge the Semaphore branch into main

21. `git push origin main`
- Push the merged changes to the remote main branch

22. `git switch Mutex`
    `git pull origin main`
- Switch to Mutex branch and pull changes from remote main

23. `git add .`
    `git commit -m "Minor code adjustments to Mutex."`
- Add changes and commit with a message

24. `git push origin Mutex`
- Push the Mutex branch to the remote repository

25. `git switch main`
    `git merge --no-ff Mutex`
- Merge the Mutex branch into main

26. `git switch Semaphore`
    `git pull origin main`
- Switch to Semaphore and pull changes from remote main

27. `git commit -m "Increased the amount of semaphores."`
- Commit changes with a message

28. `git push origin Semaphore`
- Push the Semaphore branch to the remote repository

29. `git switch main`
    `git merge --no-ff Semaphore`
- Merge the Semaphore branch into main

30. `git pull`
- Pull changes from the remote repository

31. `git switch Semaphore`
    `git add .`
    `git commit -m "Decided to implement Runnable rather than extend Thread (brings some advantages)."`
- Add changes and commit with a message

32. `git push origin Semaphore`
- Push the Semaphore branch to the remote repository

33. `git switch main`
    `git merge --no-ff Semaphore`
- Merge the Semaphore branch into main

34. `git push origin main`
- Push the merged changes to the remote main branch

35. `git pull`
- Pull changes from the remote repository

36. `git checkout -b Atomic`
- Create a new branch named 'Atomic' and switch to it

37. `git pull origin main`
- Pull changes from the remote main branch

38. `git add .`
    `git commit -m "Added Atomic."`
- Add changes and commit with a message

39. `git push origin Atomic`
- Push the Atomic branch to the remote repository

40. `git switch main`
    `git merge --no-ff Atomic`
- Merge the Atomic branch into main

41. `git push origin main`
- Push the merged changes to the remote main branch

42. `git pull origin main`
- Pull changes from the remote main branch

43. `git checkout -b PullThread`
- Create a new branch named 'PullThread' and switch to it

44. `git pull origin main`
    `git pull origin main`
- Pull changes from the remote main branch

45. `git add .`
    `git commit -m "Added PullThread."`
- Add changes and commit with a message

46. `git push origin PullThread`
- Push the PullThread branch to the remote repository

47. `git switch main`
    `git merge --no-ff PullThread`
- Merge the PullThread branch into main

48. `git push origin main`
- Push the merged changes to the remote main branch

## Результати

1.  Mutex

    <p align="center">  
        ![Рис. 2. Mutex](images/mutex.png)
    </p>

    <p align="center">
        Рис. 2. <b>Mutex</b>
    </p>

2.  Semaphore

    <p align="center">  
        ![Рис. 3. Semaphore](images/semaphore.png)
    </p>

    <p align="center">
        Рис. 3. <b>Semaphore</b>
    </p>

3.  Atomic

    <p align="center">  
        ![Рис. 4. Atomic](images/atomic.png)
    </p>

    <p align="center">
        Рис. 4. <b>Atomic</b>
    </p>

4.  PullThread

    <p align="center">  
        ![Рис. 5. PullThread](images/pullThread.png)
    </p>

    <p align="center">
        Рис. 5. <b>PullThread</b>
    </p>

# Завдання 2 (командне 75-100%)

[Lab03 Parking Lot](https://gitlab.com/rostyslav-borysenko/lab03)